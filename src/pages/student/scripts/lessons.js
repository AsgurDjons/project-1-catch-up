import { lessons, timeSlots } from "./constants";

console.log('lessons form');
let formLeson = document.querySelector('#Les');
let lesson = localStorage.getItem('Lessons') === null? []: JSON.parse(localStorage.getItem('Lessons'));
let userName = JSON.parse(localStorage.getItem('student'));
userName = userName[0].name;

formLeson.addEventListener('click', (e)=> {
    const obj = {};
    obj.name = userName;
    for (const key in timeSlots) {
        if (document.querySelector(`#${key}`).checked) {
            obj.time = timeSlots[key];
            if (parseInt(key.slice(key.length-1)) <= 3) {
                obj.tomorrow = true;
            }
            else {
                obj.tomorrow = false;
            }
        } 
    }
    for (const key in lessons) {
        if (document.querySelector(`#${key}`).checked) {
            obj.title = lessons[key].title;
            obj.duration = lessons[key].duration;
        }
    }
    lesson.push(obj);
    localStorage.setItem('Lessons', JSON.stringify(lesson));
});