import { saveToStorage } from './login';

let blocks = document.querySelectorAll('.block__login');
let regBtn = document.querySelector('#regBtn');
let toStep2Btn = document.querySelector('#toStep2Btn');
let from1to2 = document.querySelector('#from1to2');
let from2to1 = document.querySelector('#from2to1');
let count1 = document.querySelector('#toLoginSvg');
let count2 = document.querySelector('#from3to2Svg');
let count = 0;
let createAccount = document.querySelector('#createAccount');

// Кнопки ....................

function show (active = 0) {
    blocks[active].style.display = 'flex';
}

function hiden () {
    blocks.forEach(item => {
        item.style.display = 'none';
    });
}


function nextFunction() {
    hiden ();
    count++;
    show(count);
}

function prevFunction() {
    hiden();
    count--;
    show(count);
}

regBtn.addEventListener('click', ()=> {
    nextFunction();
});


toStep2Btn.addEventListener('click', (e)=> {
    nextFunction();
});

from2to1.addEventListener('click', ()=> {
    prevFunction();
});

from1to2.addEventListener('click', ()=> {
    nextFunction();
});

count1.addEventListener('click', ()=> {
    prevFunction();
});

count2.addEventListener('click', ()=> {
    prevFunction();
});

//  Валидация.......................
let form = document.querySelector('#form-user');
const userObj = {};
let localSt = localStorage.getItem('student') === null? []: JSON.parse(localStorage.getItem('student'));
let localTeach = localStorage.getItem('teacher') === null? []: JSON.parse(localStorage.getItem('teacher'));

createAccount.addEventListener('click', (e)=> {
    e.preventDefault();
    let name = document.querySelector('#name').value;
    let email = document.querySelector('#email').value;
    let password = document.querySelector('#password').value;
    let password_next = document.querySelector('#password_next').value;
    if (document.querySelector('#user_student').checked) {
        userObj.type = 'student';
    }
    if (document.querySelector('#user_teacher').checked) {
        userObj.type = 'teacher';
    }
    let validForm = ' ';
    name = name.trim();

    if (name.length >= 2 && name.length <= 25) {
        userObj.name = name;
        document.querySelector('#name').classList.remove('error');
    } else {
        validForm = undefined;
        document.querySelector('#name').classList.add('error');
    }
    if (email.includes('@')) {
        userObj.email = email;
        document.querySelector('#email').classList.remove('error');
    } else {
        validForm = undefined;
        document.querySelector('#email').classList.add('error');
    }
    if (password === password_next && password.value !== 0 && password.length >= 8) {
        userObj.password = password;
        document.querySelector('#password').classList.remove('error');
        document.querySelector('#password_next').classList.remove('error');
    } else {
        validForm = undefined;
        document.querySelector('#password').classList.add('error');
        document.querySelector('#password_next').classList.add('error');
    }
    if (validForm === undefined) {
        return;
    } else {
        form.reset();
        hiden();
        count = 0;
        show(count);
        if (userObj.type === 'student') {
            localSt.push(userObj);
            let localStudent = JSON.stringify(localSt);
            localStorage.setItem('student', localStudent);
            window.location.href = 'student.html';
        }
        if (userObj.type === 'teacher') {
            localTeach = userObj;
            let localTech = JSON.stringify(localTeach);
            localStorage.setItem('teacher', localTech);
            window.location.href = 'teacher.html';
        }
    }
});