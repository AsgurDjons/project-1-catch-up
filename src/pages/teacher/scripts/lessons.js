console.log('planed lessons');
let local = JSON.parse(localStorage.getItem('Lessons'));
let boxLes = document.querySelector('.block__scheduled-lessons');

for (const key in local) {
    let user = local[key];
    let data = Math.round(user.time + (user.duration / 60));
    let a = (data % 1 ) > 10? (data % 1): "0" + (data % 1);
    boxLes.insertAdjacentHTML('beforeend', `
    <div class="card-box">
        <div class="card-illustration">
            <img src="./images/user_03.png" alt="">
        </div>
        <div class="info">
            <p class="sub-title">${user.tomorrow? 'Сегодня': 'Завтра'}, 
            ${user.time}:00 — ${data}:${a}</p>
            <p class="info-title">${user.name}</p>
            <p class="info-desc">${user.title}</p>
        </div>
    </div>
`);
}

